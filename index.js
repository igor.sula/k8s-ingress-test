const express = require('express');

const server = express();

server.get('/', function (_req, res) {
  res.send('root');
});

server.get('/status', function (_req, res) {
  res.send('status');
});

server.get('/api', function (_req, res) {
  res.send('api');
});

server.get('/internal', function (_req, res) {
  res.send('internal');
});

const SERVER_PORT = 3000;

server.listen(SERVER_PORT, () =>
  console.log(`Server is listening on port ${SERVER_PORT}...`)
);
