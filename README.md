# K8S ingress test project

This project is created for testing K8S ingress routing with defined multiple hosts.

### How to start in locale minikube

1. start minikube

```bash
minikube start
```

for MacOS

```bash
minikube start --driver=hyperkit
```

2. enable ingress controller

```bash
minikube addons enable ingress
```

and check it

```bash
kubectl get pods -n ingress-nginx
```

you shold see `ingress-nginx-controller-<hash> 1/1 Running` after a minute.

3. push K8S configuration to minikube

```bash
kubectl apply -f k8s/deployment.yml
```

and wait to application pods are created successfuly

4. add hosts for your local machine

- check your ingress IP adress in `ADDRESS` column

```bash
kubectl get ingress
```

if you see a localhost in `ADDRESS` columb try

```bash
minikube ip
```

- edit your local `/etc/hosts`

```bash
sudo vi /etc/hosts
```

and add these rows:

`<ingress_ip_adress> k8s-test.private.info`

`<ingress_ip_adress> k8s-test.public.info`

For example:

`192.168.64.3 k8s-test.private.info`

`192.168.64.3 k8s-test.public.info`

5. try to request ingress

```bash
curl k8s-test.private.info/internal
curl k8s-test.public.info
curl k8s-test.public.info/api
curl k8s-test.public.info/status
```

These instructions are inspired from [this](https://kubernetes.io/docs/tasks/access-application-cluster/ingress-minikube/) article.
